---
layout: default1
title: Changelog
permalink: /changelog
---

---
# v1.05

- New GrapheneOS page on ``/graphene``

- New F-Droid page on ``/f-droid``

- Updated Qubes OS content on ``/qubes``

- Update various formatting

- Added Briar on ``/guide``

- New "Physical" section on ``/guide``

- New ``/snowflake`` page

- New ``/help`` page

- Additional content on ``/information`` for products

- More content on ``/guide`` for secure hardware

- New articles & videos on ``/guide`` for facial recognition and thermal imaging

# v1.04

- Fixed various naming schemes

- Removed NoScript, LocalCDN and CanvasBlocker from ``/tools``

- Added Bitwarden to ``/tools``

- Update page list on ``/index``

- Update ``/services`` page for better formatting and wording

- Updated ``/RSS`` page

- New ``/propagate`` page

- Update button layout on ``/index``

- Added favicon

- Add disclaimer for Duckduckgo and Yubikey on ``/tools``

# v1.03

- Removed update notice

- Updated resource links on ``/guide``

- Updated several typographical errors

- Added icons to index

- New ``/matrix`` page

- New browser section on ``/guide``

- Additional content regarding secure messengers on ``/guide``

- Updated various spacing and wording on ``/tools``

- Update spacing on ``index``

# v1.02

- Update ``index`` to include page list

- Include ``research`` section inside ``/information``

- Added self-hosted Element instance

- New ``/qubes`` page for notes reguarding QubesOS

- Additional Kicksecure documentation on ``/qubes``

- Update ``services`` page

- New Forums

- Added Mega research URL

# v1.01

- Additional content for aliasing in ``/guide``

- Update formatting and wording in ``/tools``

- Include the preface and footnotes in ``/guide``

- Update desktop security model information in ``/guide``

- Formatting changes in ``/guide``

- New blog post

- Updated titles in ``/rss``

- Matrix IP logging reduced to 30 seconds

- New ``information`` page which will include articles, projects, etc.

- Several typographical errors fixed

- RSS feed for ``items``

# v1.0

- Added new page ``Services.md``

- Reformatting old blog post

- New Jekyll plugin ``jekyll-feed``

- New Jekyll plugin ``jekyll-seo-tag``

- New ``_changelog`` folder along with ``changelog.md``

- Fix formatting error on ``Services.md``

- Added RSS feed for ``posts`` and for ``changelog``

- Updated file naming to be consistent

- Reformat buttons on ``index.md``
