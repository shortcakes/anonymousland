---
layout: default1
description: List of services
title: Services
permalink: /services
---

<div style="text-align:center;">
<i>“There are too many of us, he thought. There are billions of us and that's too many. Nobody knows anyone. Strangers come and violate you. Strangers come and cut your heart out. Strangers come and take your blood. Good God, who were those men? I never saw them before in my life!” </i>
</div>

### __Services__

These services are publicly available and free to use. 

Services are hosted in __Sweden__ and / or __Iceland__.

The hosting providers [1984](https://1984.is) and [Njalla](https://njal.la) are in use.

You can check the status of our Matrix Homeserver and all off our other services on our [status page](https://status.anonymousland.org/status/services).
<br>

### __Terms of Service (ToS)__

These services are not intended to spread spam or malicious content, or sharing / distributing / hosting of malicious content.

Your account *can* be suspended if you take part in any sort of malicious activity or questionable content including but limited to targeted harassment, raids, phishing, DOXxing or violating laws of __Sweden__ and __Iceland__.

### __Privacy & Security policy__

As nature of this website, privacy and security must be our main priority, the following is currently in effect:

- nginx logging is disabled on all servers.

<br>

#### Matrix

- Registration is open, if abuse takes place, registration will turn to an invite-only system.

- Registered users are automatically joined into the lounges, this is to help prevent abuse!

- Guest access is currently enabled, and will be disabled if abuse takes place.

- Moderation is done via ``mjolnir``, let us keep this ban list to a minimum.

- Ratelimiting actions is currently set as default on Matrix, and can be easily re-enabled if needed, do not ruin this for others.

- We are currently not using a CAPTCHA for registration or requiring an Email, CAPTCHA will be enabled if abuse happens!

- Metrics are disabled.

- Federation is enabled.

- URL previews are allowed.

- Synapse is hosted with [1984](https://1984.hosting/GDPR/)

- `user_ips_max_age` is currently set to `7d`

- `bcrypt_rounds` is set to `15`

- `MSC2285` is enabled

<br>

### __Policies__

Your data and information will **NEVER** be sold or shared to another party. 

Matrix federation may cause some information to be sent to third party servers as this is how federation functions.

Your data and information will **NEVER** be handed away to *anyone*.

We try to collect as little information as possible, to protect *everyone*.

Additional services are hosted with [Njalla](https://njal.la/about/).

<br>

#### __Matrix__

The Matrix homeserver is located at

`https://matrix.anonymousland.org`

We also have our self-hosted Element instance located at

`https://element.anonymousland.org`

Incidents: ``1``

Password Policy:

- 15 characters

- Require at least 1 digit

- Require at least 1 symbol

- At least 1 lowercase character

- At least 1 uppercase character

<br>

  <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Metadata</h3>
      </div>
      <div class="panel-body">
        Due to how the Matrix protocol functions, metadata including your Profile picture, username, display name and device list will be sent to other homeservers if you are in a federated room.
        An IP address is automatically assigned with each client, signing out will remove your IP from the client and from the database as well.
      </div>
    </div>

<br>

## __Additional Services__

- [Cinny](https://cinny.anonymousland.org)
- [Forum](https://forum.anonymousland.org)
- [Hydrogen](https://hydrogen.anonymousland.org)
