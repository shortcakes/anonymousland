---
layout: default1
title: Anonymousland
description: A fun happy place.
---

<div style="text-align:center;">
<!-- https://invidious.kavin.rocks/watch?v=J-6fW66IUY4 -->
<h1>The happiest place on earth!</h1>
<h2>(We’re all anonymous here)</h2>
<br>
<p><i>“You can keep as quiet as you like, but one of these days somebody is going to find you.” </i></p>
<br>
<br>
</div>

---

<div style="text-align:center">
<br>
<br>

  <button type="button" class="btn btn-lg btn-default"><a href="./changelog"><i class="fa fa-clipboard" aria-hidden="true"></i> Changelog v1.0.5</a></button>

<br>
<br>

  <button type="button" class="btn btn-md btn-default"><a href="https://codeberg.org/deathrow/anonymousland"> <i class="fa fa-github" aria-hidden="true"></i> Repository</a></button>
  
  <button type="button" class="btn btn-md btn-default"><a href="https://github.com/d-eathrow/anonymousland"> <i class="fa fa-github" aria-hidden="true">
    </i> GitHub mirror</a></button>

<br>
<br>

  <button type="button" class="btn btn-md btn-default"><a href="./rss"><i class="fa fa-rss-square" aria-hidden="true"></i> RSS</a></button>

<br>
<br>

  <button type="button" class="btn btn-md btn-default"><a href="./information"><i class="fa fa-list" aria-hidden="true"></i> Collections</a></button>


<br>
<br>
<br>

  <button type="button" class="btn btn-lg btn-default"><a href="https://status.anonymousland.org"> Status Page</a></button>

  <button type="button" class="btn btn-lg btn-default"><a href="./services"> Services Page</a></button>
<br>
<br>
<br>

  <button type="button" class="btn btn-md btn-default"><a href="https://cinny.anonymousland.org"> Cinny</a></button>


  <button type="button" class="btn btn-md btn-default"><a href="https://element.anonymousland.org"> Element</a></button>
 
  <button type="button" class="btn btn-md btn-default"><a href="https://hydrogen.anonymousland.org"> Hydrogen</a></button>

  <button type="button" class="btn btn-md btn-default"><a href="https://forum.anonymousland.org"><i class="fa fa-commenting" aria-hidden="true"></i> Forums</a></button>

<br>
<br>

  </div>

<br>
<br>


## Site Layout

- [Anonymousland](https://anonymousland.org)
  - [Changelog](./changelog)
  - [Donate](./donate)
  - [Guide](./guide)
  - [Help](./help)
  - [Information](./information)
     - [GrapheneOS](./graphene)
     - [F-Droid](./f-droid)
     - [Matrix](./matrix)
     - [Qubes OS](./qubes)
  - [Propagation](./propagate)
  - [RSS](./rss)
  - [Tools](./tools)
  - [Services](./services)
  - [Snowflake](./snowflake)