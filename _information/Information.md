---
layout: default1
description: Information
title: Information
permalink: /information
---

A collection of links, articles, resources and more. [<i class="fa fa-rss" aria-hidden="true"></i>](https://anonymousland.org/feed/information.xml)



<br>

#### Table of contents:


-\>> [Main Collection](#main-collection) <br>
  - -\> *[News](#news)* <br>
  - -\> *[Articles](#articles)* <br>
  - -\> *[Communities](#communities)* <br>
  - -\> *[Products](#products)* <br>
  - -\> *[Projects](#projects)* <br>
  - -\> *[Research](#research)* <br>
  - -\> *[Random](#random)* <br>

<br>

# Main Collection

<br>

## News

For news

- [China AI image generator](https://arstechnica.com/information-technology/2022/09/chinas-leading-ai-image-generator-nixes-political-content-surprising-no-one/)

- [China targeting human rights](https://www.technologyreview.com/2022/08/16/1057894/hackers-linked-to-china-have-been-targeting-human-rights-groups-for-years/)


<br>


## Articles

Articles and stories related to technology, security or privacy

- [Your smartphone could recognize you by the way you hold it](https://www.newscientist.com/article/2334048-your-smartphone-could-recognise-you-just-by-the-way-you-hold-it/)

- [Intel thinks AI knows what students think and feel](https://www.protocol.com/enterprise/emotion-ai-school-intel-edutech)

- [Anti-fingerprinting techniques](https://fingerprint.com/blog/browser-anti-fingerprinting-techniques/)

- [Who is collecting data from your car](https://themarkup.org/the-breakdown/2022/07/27/who-is-collecting-data-from-your-car)

- [Hacking Police Body Cameras](https://www.wired.com/video/watch/hacking-police-body-cameras)
<br>

## Communities

A place for related various communities & media

- [PrivacyGuides](https://privacyguides.org) <button type="button" class="btn btn-xs btn-xs"><a href="http://eter4u55b667kuo72ntpm7ut54sa2mxmr22iqgzns4jw7boeox3qgyid.onion">Tor</a></button>

- [Privsec](https://privsec.dev)

- [Privacy.do](https://privacy.do)

## Products

Items you can buy
*(These are not affiliated or sometimes even recommended for some cases.
This is simply a list.
Do your own research).*

- [Reflectables](https://reflectables.com)

- [URME Surveillance](https://www.urmesurveillance.com/)

- [Unpickable](https://ominoushum.com/lock/)

<br>

## Projects

A list of interesting projects

- [HDMI Firewall](https://git.cuvoodoo.info/kingkevin/board/src/branch/hdmi_firewall/README.md)

- [DEDA](https://github.com/dfd-tud/deda)

<br>

## Research

For research-based articles

- [Practically-exploitable Cryptographic Vulnerabilities in Matrix](https://nebuchadnezzar-megolm.github.io/)

- [Memetic Warfare](https://www.academia.edu/43534914/Memetic_Warfare_The_Future_of_War)

- [Mega.nz Encryption](https://mega-awry.io/)

## Random

For anything that does not meet the above category

- [Prepper Handboook](https://wiki.jameskitt616.one/en/bugout-handbook)

- [Next Generation Data Storage](https://foliophotonics.com/)

- [World Map of Encryption](https://www.gp-digital.org/world-map-of-encryption/)

- [Logic Sentences](https://sive.rs/1s)
